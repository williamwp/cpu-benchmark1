IF(NOT EXISTS "/home/wangpeng/tools/3d/5/model/build/external/assimp/install_manifest.txt")
  MESSAGE(FATAL_ERROR "Cannot find install manifest: \"/home/wangpeng/tools/3d/5/model/build/external/assimp/install_manifest.txt\"")
ENDIF(NOT EXISTS "/home/wangpeng/tools/3d/5/model/build/external/assimp/install_manifest.txt")

FILE(READ "/home/wangpeng/tools/3d/5/model/build/external/assimp/install_manifest.txt" files)
STRING(REGEX REPLACE "\n" ";" files "${files}")
FOREACH(file ${files})
  MESSAGE(STATUS "Uninstalling \"$ENV{DESTDIR}${file}\"")
  EXEC_PROGRAM(
    "/home/wangpeng/tools/3d/49/cmake-3.20.0-rc3/bin/cmake" ARGS "-E remove \"$ENV{DESTDIR}${file}\""
    OUTPUT_VARIABLE rm_out
    RETURN_VALUE rm_retval
    )
  IF(NOT "${rm_retval}" STREQUAL 0)
    MESSAGE(FATAL_ERROR "Problem when removing \"$ENV{DESTDIR}${file}\"")
  ENDIF(NOT "${rm_retval}" STREQUAL 0)
ENDFOREACH(file)
