
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/wangpeng/tools/3d/5/model/external/glad/src/glad.c" "CMakeFiles/Task-model.dir/external/glad/src/glad.c.o" "gcc" "CMakeFiles/Task-model.dir/external/glad/src/glad.c.o.d"
  "/home/wangpeng/tools/3d/5/model/external/stb/stb_image.cpp" "CMakeFiles/Task-model.dir/external/stb/stb_image.cpp.o" "gcc" "CMakeFiles/Task-model.dir/external/stb/stb_image.cpp.o.d"
  "/home/wangpeng/tools/3d/5/model/src/Camera.cpp" "CMakeFiles/Task-model.dir/src/Camera.cpp.o" "gcc" "CMakeFiles/Task-model.dir/src/Camera.cpp.o.d"
  "/home/wangpeng/tools/3d/5/model/src/Mesh.cpp" "CMakeFiles/Task-model.dir/src/Mesh.cpp.o" "gcc" "CMakeFiles/Task-model.dir/src/Mesh.cpp.o.d"
  "/home/wangpeng/tools/3d/5/model/src/Model.cpp" "CMakeFiles/Task-model.dir/src/Model.cpp.o" "gcc" "CMakeFiles/Task-model.dir/src/Model.cpp.o.d"
  "/home/wangpeng/tools/3d/5/model/src/Shader.cpp" "CMakeFiles/Task-model.dir/src/Shader.cpp.o" "gcc" "CMakeFiles/Task-model.dir/src/Shader.cpp.o.d"
  "/home/wangpeng/tools/3d/5/model/src/Texture.cpp" "CMakeFiles/Task-model.dir/src/Texture.cpp.o" "gcc" "CMakeFiles/Task-model.dir/src/Texture.cpp.o.d"
  "/home/wangpeng/tools/3d/5/model/src/main.cpp" "CMakeFiles/Task-model.dir/src/main.cpp.o" "gcc" "CMakeFiles/Task-model.dir/src/main.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wangpeng/tools/3d/5/model/build/external/assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/home/wangpeng/tools/3d/5/model/build/external/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/home/wangpeng/tools/3d/5/model/build/external/assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
