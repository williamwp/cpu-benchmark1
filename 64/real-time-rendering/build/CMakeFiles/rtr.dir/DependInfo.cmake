
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/gui/gui.cc" "CMakeFiles/rtr.dir/src/gui/gui.cc.o" "gcc" "CMakeFiles/rtr.dir/src/gui/gui.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/image/image.cc" "CMakeFiles/rtr.dir/src/image/image.cc.o" "gcc" "CMakeFiles/rtr.dir/src/image/image.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/main.cc" "CMakeFiles/rtr.dir/src/main.cc.o" "gcc" "CMakeFiles/rtr.dir/src/main.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/parser/obj-parser.cc" "CMakeFiles/rtr.dir/src/parser/obj-parser.cc.o" "gcc" "CMakeFiles/rtr.dir/src/parser/obj-parser.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/parser/options.cc" "CMakeFiles/rtr.dir/src/parser/options.cc.o" "gcc" "CMakeFiles/rtr.dir/src/parser/options.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/program/program.cc" "CMakeFiles/rtr.dir/src/program/program.cc.o" "gcc" "CMakeFiles/rtr.dir/src/program/program.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/scene/camera.cc" "CMakeFiles/rtr.dir/src/scene/camera.cc.o" "gcc" "CMakeFiles/rtr.dir/src/scene/camera.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/scene/light.cc" "CMakeFiles/rtr.dir/src/scene/light.cc.o" "gcc" "CMakeFiles/rtr.dir/src/scene/light.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/scene/observer.cc" "CMakeFiles/rtr.dir/src/scene/observer.cc.o" "gcc" "CMakeFiles/rtr.dir/src/scene/observer.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/scene/triangle.cc" "CMakeFiles/rtr.dir/src/scene/triangle.cc.o" "gcc" "CMakeFiles/rtr.dir/src/scene/triangle.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/shader/shader.cc" "CMakeFiles/rtr.dir/src/shader/shader.cc.o" "gcc" "CMakeFiles/rtr.dir/src/shader/shader.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/texture/texture.cc" "CMakeFiles/rtr.dir/src/texture/texture.cc.o" "gcc" "CMakeFiles/rtr.dir/src/texture/texture.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/utils/point3.cc" "CMakeFiles/rtr.dir/src/utils/point3.cc.o" "gcc" "CMakeFiles/rtr.dir/src/utils/point3.cc.o.d"
  "/home/wangpeng/tools/3d/64/real-time-rendering/src/utils/vector3.cc" "CMakeFiles/rtr.dir/src/utils/vector3.cc.o" "gcc" "CMakeFiles/rtr.dir/src/utils/vector3.cc.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
