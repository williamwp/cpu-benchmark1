
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/box.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/box.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/box.cpp.o.d"
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/compositeShape.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/compositeShape.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/compositeShape.cpp.o.d"
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/mengerSponge.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/mengerSponge.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/mengerSponge.cpp.o.d"
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/mesh.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/mesh.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/mesh.cpp.o.d"
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/meshTriangle.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/meshTriangle.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/meshTriangle.cpp.o.d"
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/plane.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/plane.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/plane.cpp.o.d"
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/rectangle.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/rectangle.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/rectangle.cpp.o.d"
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/sphere.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/sphere.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/sphere.cpp.o.d"
  "/home/wangpeng/tools/3d/47/render_engine-master/src/scene/geometry/triangle.cpp" "src/scene/geometry/CMakeFiles/geometry.dir/triangle.cpp.o" "gcc" "src/scene/geometry/CMakeFiles/geometry.dir/triangle.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wangpeng/tools/3d/47/render_engine-master/build/src/scene/materials/CMakeFiles/materials.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
